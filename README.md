# TzScan

TzScan is a block explorer for the Tezos blockchain.

TzScan has been developed by [OCamlPro](http://www.ocamlpro.com) since
September 2017.

## Issues and Contributions

Check the project on [Gitlab.com](https://gitlab.com/tzscan/tzscan)
for the latest sources, issues and contributions:

* Issues: Check the [Issues](https://gitlab.com/tzscan/tzscan/issues)
  and submit a new one !
* Contributions: Check the
  [Contributions](https://gitlab.com/tzscan/tzscan/merge_requests)
  and submit your MR !

## Copyright and license

Copyright OCamlPro 2017-2019. This code is licensed under the terms
of the GNU Public License version 3 (GPL v3).

## Building

You need OCaml 4.07.1.

Update submodules:

```
git submodule init
git submodule update
```

### Dependencies

TzScan has multiple OCaml and non OCaml dependencies.
The non OCaml dependencies must be installed first as they are necessary
for the OCaml ones.

#### Non OCaml dependencies

On Debian 9:
```
sudo apt-get install\
     postgresql libsodium-dev libgeoip1 \
     geoip1-database libcurl4-gnutls-dev \
     curl zlib1g-dev libgeoip-dev
```

On Ubuntu:
```
sudo apt-get install\
     postgresql libsodium-dev libgeoip1 \
     geoip-database libcurl4-gnutls-dev \
     curl zlib1g-dev libgeoip-dev
```

On CentOS 7 (utdrmac):

Base packages:
```
yum install epel-release
yum install gcc m4 make libsodium libsodium-devel geoip geoip-devel zlib libcurl libcurl-devel
```

PostgreSQL 10:
```
rpm -Uvh https://yum.postgresql.org/10/redhat/rhel-7-x86_64/pgdg-centos10-10-2.noarch.rpm
yum install postgresql10 postgresql10-server postgresql10-libs
```

#### OCaml dependencies

Before installing and compiling all dependencies, you need to disable
the sandbox mode of opam. Check the file `~/.opam/config`, and comment
the lines below if there are still there:

```
wrap-build-commands:
  ["%{hooks}%/sandbox.sh" "build"] {os = "linux" | os = "macos"}
wrap-install-commands:
  ["%{hooks}%/sandbox.sh" "install"] {os = "linux" | os = "macos"}
wrap-remove-commands:
  ["%{hooks}%/sandbox.sh" "remove"] {os = "linux" | os = "macos"}
```

When you finished the compilation, you can reenable the sandboxing
mode of opam.

Then the dependencies can (and should) be installed with opam through

```
opam install \
     base64.3.2.0 \
     cohttp-lwt-unix.2.0.0 \
     csv-lwt.2.2 \
     ezjsonm.1.1.0 \
     geoip.0.0.3 \
     js_of_ocaml-camlp4.3.1.0 \
     js_of_ocaml-lwt.3.4.0 \
     js_of_ocaml-ppx.3.4.0 \
     js_of_ocaml-tyxml.3.4.0 \
     lwt_log.1.1.0 \
     cstruct.4.0.0 \
     nocrypto.0.5.4-1 \
     ocplib-endian.1.0 \
     ocurl.0.8.2 \
     omd.1.3.1 \
     sodium.0.6.0 \
     ocp-build.1.99.20-beta \
     ocplib-json-typed.0.7.1 \
     alcotest.0.8.5 \
     pgocaml.3.2 \
     extunix.0.1.6
```

You also need to install the development version of httpaf and httpaf-lwt-unix:
```
./build-httpaf.sh
```

Optional:
If you don't want to disable the sandbox mode in opam, you can also build pgocaml from the sources using:
```
./rebuild-pgocaml.sh
```

### Configuring

#### Configuring Postgresql

For TzScan to compile successfully, you will need your user to be able
to create tables in Postgresql. This is done with the following
commands: ``` $ sudo -i -u postgres $ psql CREATE USER <user>; ALTER
ROLE <user> CREATEDB; ``` where `<user>` should be replaced by your
login.


#### Configuring TzScan

Then, configure TzScan:
```
./configure
```

You can create a file `Makefile.database` with the following options:

```
DATABASE=betanet
DESTSUBDIR=tzscan
WITH_VERSION=true
API_HOST:=localhost
API_PORT:=8080
NEED_PARSEXP=true
BASE64_3:=true
```

The options are:
* DATABASE: the database to use (you can use different names for other networks
    or tests)
* DESTSUBDIR: only used on the production server (the destination subdir
    in /var/www)
* WITH_VERSION: set to false to avoid recompiling everything everytime
* API_HOST & API_PORT: if you want to use localhost instead of api.tzscan.io
* NEED_PARSEXP: if you use sexplib (>= v0.11), then set it to true
* BASE64_3: if you use base64 (v>=3.0.0), then set it to true

### Building

You can now start the compilation:
```
make
```

If no error happened, you should get:
* A binary called `tzscan-crawler`
* A binary called `tzscan-api-server`
* A directory `www` containing at least an `explorer-main.js` file

## Usage

### Overview

TzScan is composed of the following components:
* A Tezos crawler, called `tzscan-crawler`. It regularly connects to a
  Tezos node to query new blocks, and register these blocks in a Postgresql
  database;
* An API server, called `tzscan-api-server`. It provides a REST RPC that can
  be used to read information from the Postgresql database. Some of the requests
  are also used to query local Tezos nodes;
* A webapp, stored in `www/`. The webapp runs some Javascript code to
  display information in a browser.

There are mostly two ways to contribute to TzScan:
* You can just use the webapp. For that, you just need a local web server.
  By default, it will connect to the official TzScan API server. If you
  do modifications, you can immediately check the results without running
  your own Tezos node.
* You can run a full TzScan node: you need to run a Tezos node,
  to run TzScan `tzscan-crawler` to fill the database, to run
  `tzscan-api-server` to give access to the database, and to run a
  local web server for the webapp.

### Testing the Interface Locally

You need to run a Web Server, that will give access to the `www/` directory
of the webapp. A simple way is to use the web server included in PHP:

First, install PHP:
```
sudo apt-get install php
```

Go into `www/`:
```
cd www
php -S localhost:8000
```

and view the webpage:
```
xdg-open http://localhost:8000/
```

If you didn't modify `API_PORT` and `API_HOST` in `Makefile.database`,
the webapp will use the official TzScan API Server. Otherwise, it will
try to connect to your API server.

Note that you edit `static/info.json` (that will be copied to
`www/info.json`) to change some informations on your web server, such
as the URL of the API server, the languages available or the networks
you are tracking.

### Filling the Database with the Crawler

Create a file `config.json` containing :
```
[
  {
    "name": "mainnet-main",
    "crawler" : [
        {"url" : "http://tezos-node", "port" : 8732}
    ],
    "api" : [
        {"url" : "http://tezos-node", "port" : 8732},
    ]
  } ,

  {
    "name" : "mainnet-extra-data",
    "crawler" : [
        {"url" : "http://tezos-node", "port" : 8732}
    ],
    "api" : [ ],
    "max_default_blocks" : 10,
    "max_default_operations" : 10
  }
]
```

Start the crawler:
```
./tzscan-crawler config.json
```

It should immediately connect to the Tezos node, and rewind the chain
to register blocks from the genesis block to the latest one. Then, it
should wait and connect to the Tezos node every 3 seconds to query for new
blocks.

To get extra non-critical data like marketcap data or voting data, you
should run another instance of the crawler with `--extra-data` option

```
./tzscan-crawler --extra-data config.json
```

This instance of the crawler will read the `$NETWORK-extra-data` field
from your config.json

### Running the API Server Locally

You need first to have started the Tezos crawler as explained in the
previous section.

Modify file `Makefile.database` with:

```
API_PORT = 8080
API_HOST = localhost
```

and rebuild everything:
```
make
```

Start the API server (on port 8080):
```
./tzscan-api-server --node-config config.json --api-config www/api-config.json
```

Note that you might want to change `www/api-config.json` (and its
source in `static/api-config.json`) to suit the parameters of your
network, if you are running a private Tezos network. For example, you
can use:

```
tezos-client -A localhost -p 8132 rpc get /chains/main/blocks/head/context/constants
```

to get the constants of your network, and modify `api-config.json`
accordingly.

Note that, if you need to modify `static/info.json` or
`static/api-config.json`, it is usually a good idea to copy them in
`static/local/` and modify these files. They will be copied in `www/`
at every `make` command.
